#!/bin/bash

if [ -f /secret/password ]; then
  pass=`cat /secret/password`
  usermod -p `openssl passwd -1 $pass` jump
fi

cat <<EOF
-------------------------------------
GID/UID
-------------------------------------
User uid:    $(id -u jump)
User gid:    $(id -g jump)
-------------------------------------
EOF

cp -r /ssh-configs/* /etc/ssh

/usr/sbin/sshd -D -p 22