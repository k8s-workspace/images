# centos 8
FROM centos:latest 
ENV LANG=en_US.utf8
ARG home_dir=/home/default

RUN yum -y update && \
    yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm

RUN yum -y install \
    vim git make ca-certificates curl sudo openssh-server \
    file passwd gcc-c++ gcc cracklib-dicts python3 \
    zsh glibc-locale-source glibc-langpack-en

RUN echo "$LANG UTF-8" >> /etc/locale.gen
RUN localedef --force -i en_US -f UTF-8 en_US.UTF-8

RUN mkdir -p /var/run/sshd
RUN ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N ''
RUN sshd -t

RUN mkdir /home/default
RUN groupadd -g 1001 default
RUN useradd -rm -d /home/default -s /bin/zsh -g 1001 -G wheel -u 1001 default

RUN git clone https://github.com/Homebrew/brew /home/default/.linuxbrew/Homebrew
RUN mkdir /home/default/.linuxbrew/bin
RUN ln -s ../Homebrew/bin/brew /home/default/.linuxbrew/bin/
RUN /home/default/.linuxbrew/bin/brew config
RUN /home/default/.linuxbrew/bin/brew install hello || true

RUN rm -rf /run/nologin
RUN cp -r /etc/ssh /ssh-configs

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]