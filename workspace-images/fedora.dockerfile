FROM fedora:latest
ENV LANG=en_US.utf8
ARG home_dir=/home/default

RUN dnf -y install \
    vim git make ca-certificates curl sudo openssh-server \
    file passwd gcc-c++ gcc glibc-common cracklib-dicts python3 \
    zsh glibc-locale-source glibc-langpack-en libxcrypt-compat \
    openssl procps

RUN echo "$LANG UTF-8" >> /etc/locale.gen
RUN localedef --force -i en_US -f UTF-8 en_US.UTF-8

RUN mkdir -p /run/sshd
RUN ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key -N ''
RUN sshd -t

RUN mkdir /home/default
RUN groupadd -g 1001 default
RUN useradd -rm -d /home/default -s /bin/zsh -g 1001 -G wheel -u 1001 default

RUN git clone https://github.com/Homebrew/brew /home/linuxbrew/.linuxbrew/Homebrew
RUN mkdir /home/linuxbrew/.linuxbrew/bin
RUN ln -s ../Homebrew/bin/brew /home/linuxbrew/.linuxbrew/bin/
RUN /home/linuxbrew/.linuxbrew/bin/brew config
RUN /home/linuxbrew/.linuxbrew/bin/brew install hello || true

RUN mv /home/linuxbrew /linuxbrew

RUN cp -r /etc/ssh /ssh-configs

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]