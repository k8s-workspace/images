# latest LTS
FROM ubuntu:latest 

RUN ln -fs /usr/share/zoneinfo/Canada/Atlantic /etc/localtime

ARG DEBIAN_FRONTEND=noninteractive 
ARG home_dir=/home/default

RUN apt-get update --fix-missing -y && \
    apt-get upgrade -y && \
    apt-get install --no-install-recommends -y \
    vim git make ca-certificates curl sudo openssh-server \
    file g++ locales uuid-runtime \
    zsh

RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
    && dpkg-reconfigure locales \
    && update-locale LANG=en_US.UTF-8

RUN mkdir -p /run/sshd
RUN sshd -t

RUN groupadd -g 1001 default
RUN useradd -rm -d /home/default -s /bin/zsh -g 1001 -G sudo -u 1001 default

RUN git clone https://github.com/Homebrew/brew /home/default/.linuxbrew/Homebrew
RUN mkdir /home/default/.linuxbrew/bin
RUN ln -s ../Homebrew/bin/brew /home/default/.linuxbrew/bin/
RUN /home/default/.linuxbrew/bin/brew config
RUN /home/default/.linuxbrew/bin/brew install hello

RUN cp -r /etc/ssh /ssh-configs

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]