#!/bin/bash

if [ -f /secret/password ]; then
  pass=$(cat /secret/password)
  usermod -p $(openssl passwd -1 $pass) default
fi

if [ ! -f /etc/ssh/ssh_host_dsa_key ]; then
  cp -r /ssh-configs/* /etc/ssh
  rm -rf /etc/ssh/ssh_host_*
  ssh-keygen -A
fi

USER_NAME=${USER_NAME:-default}

PUID=${PUID:-1001}
PGID=${PGID:-1001}

[[ "$USER_NAME" != "default" ]] && \
    usermod -l "$USER_NAME" default && \
    groupmod -n "$USER_NAME" default

groupmod -o -g "$PGID" "$USER_NAME"
usermod -o -u "$PUID" "$USER_NAME"

home_dir="/home/$USER_NAME"
usermod -d $home_dir $USER_NAME

if [ ! -f $home_dir/.initialized ]; then
  if [ ! -d /home/linuxbrew ]; then
    mv /linuxbrew /home/linuxbrew
  fi

  chown -R $PUID:$PGID $home_dir
  chown -R $PUID:$PGID /home/linuxbrew/.linuxbrew

  if [ ! -f $home_dir/.zshrc ]; then
    su $USER_NAME -c 'sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattende'
  fi

  touch $home_dir/.initialized
fi

export LANG=en_US.UTF-8
export SHELL=/bin/zsh

export PATH=/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:$PATH

if ! grep -q linuxbrew $home_dir/.zshrc; then
  test -d /home/linuxbrew/.linuxbrew && eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
  test -d /home/linuxbrew/.linuxbrew && echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>$home_dir/.bash_profile
  test -d /home/linuxbrew/.linuxbrew && echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>$home_dir/.zshrc
  test -d /home/linuxbrew/.linuxbrew && echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>$home_dir/.profile
fi

cat <<EOF
-------------------------------------
GID/UID
-------------------------------------
User uid:    $(id -u "$USER_NAME")
User gid:    $(id -g "$USER_NAME")
-------------------------------------
EOF

/usr/sbin/sshd -D -p 22
