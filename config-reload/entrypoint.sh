#!/bin/sh

sleep 5

PUID=${PUID:-1001}
PGID=${PGID:-1001}

DOT_SSH=${DOT_SSH:-/.ssh}

if [ ! -d $DOT_SSH ]; then
	mkdir -p $DOT_SSH
fi

cp /auth-keys-secrets/authorized_keys $DOT_SSH/authorized_keys
chmod 700 $DOT_SSH
chown -R "$PUID":"$PGID" $DOT_SSH

cp /sshd-config/sshd_config /ssh
chmod 644 /ssh/sshd_config

while inotifywait \
	-e modify \
	-e create \
	-e delete \
	-e move \
-qqr /auth-keys-secrets /sshd-config; do
	cp /auth-keys-secrets/authorized_keys $DOT_SSH/authorized_keys
	chown -R "$PUID":"$PGID" $DOT_SSH
	echo 'authorized_keys updated'

	cp /sshd-config/sshd_config /ssh
	chmod 644 /ssh/sshd_config
	echo 'sshd_config updated'

	p=$(ps aux)
	pid=$(echo "$p" | grep '/usr/sbin/sshd -D -p 22' | grep -v 'bin/sh' | awk '{print $1}')
	kill -SIGHUP "$pid"
done